﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class GameManager
    {
        private Character _player;
        private Character _enemy;
        private int _depth;
        private bool _gameOver;
        private int _enemyDamage;
        private int _playerDamage;
        private RandomEnemyGenerator enemyGenerator;

        public Character Player { get { return _player; } }
        public Character Enemy { get {return _enemy; } }
        public int Depth { get {return _depth; } }
        public bool GameOver { get {return _gameOver; } }
        public int EnemyDamage { get { return _enemyDamage; } set { _enemyDamage = value; } }
        public int PlayerDamage { get { return _playerDamage; } set { _playerDamage = value; } }

        public GameManager()
        {
            _player = new Character(50, "The Dude");

            _player.Equipped.Equip(InventorySlotId.WEAPON, new BronzeSword());
            _player.Equipped.Equip(InventorySlotId.CHESTPIECE, new Gambeson());
            _player.Equipped.Equip(InventorySlotId.POTION1, new SmallPotion());

            enemyGenerator = new RandomEnemyGenerator();
            _enemy = enemyGenerator.GenerateRandomEnemy(_depth);
            _depth = 1;
        }

        public void ShowGameOver()
        {
            Game_Over gameOver = new Game_Over();
            gameOver.ShowDialog();
        }

        public void Attack()
        {
            Random random = new Random();
            int tieDamage = random.Next(0,3);
            int playerAtk = _player.CalcTotalAttackValue();
            int playerDef = _player.CalcTotalDefenseValue();
            int enemyAtk = _enemy.CalcTotalAttackValue();
            int enemyDef = _enemy.CalcTotalDefenseValue();


            if (playerAtk > enemyDef)
            {
                _enemy.TakeDamage(playerAtk - enemyDef);
            }
            else
            {
                _enemy.TakeDamage(tieDamage);
            }
            if (_enemy.CurrentHealth > 0)
            {
                if (enemyAtk > playerDef)
                {
                    _player.TakeDamage(enemyAtk - playerDef);
                }
                else
                {
                    _player.TakeDamage(tieDamage);
                }
            }
            else
            {
                Enemy.UnequipAll();
                IncreaseDepth();
            }
        }

        public void GenerateEnemy()
        {
            _enemy = enemyGenerator.GenerateRandomEnemy(_depth);
        }

        public void IncreaseDepth()
        {
            ++_depth;
        }

        public void Heal()
        {    
            Item item = _player.Equipped.GetItem(InventorySlotId.POTION1);
            if (item is IPotion)
            {
                IPotion potion = (IPotion)item;
                _player.TakeDamage(-potion.HealValue);
            }
        }
    }
}
