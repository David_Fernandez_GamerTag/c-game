﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public abstract class Item : IComparable
    {
        public Guid Id { get; }
        public abstract string Name { get; }
        public abstract double Weight { get; }
        public abstract InventorySlotId Slot { get; }
        public abstract bool IsNatural { get; }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return (Name);
        }

        //public static Item operator == (Item a, Item b)
        //{
        //    return null;
        //}

        //public static Item operator != (Item a, Item b)
        //{
        //    return null;
        //}
    }
}
