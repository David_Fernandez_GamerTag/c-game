﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class SmallPotion : Item, IPotion
    {
        private int _healValue;

        public int HealValue { get { return _healValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 2; } }
        public override string Name { get { return "Potion"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.POTION1; } }
        //public override InventorySlotId Slot { get { return InventorySlotId.POTION2; } }

        public SmallPotion()
        {
            Random random = new Random();
            _healValue = random.Next(10, 16);
        }

        public override string ToString()
        {
            return base.ToString() + " Heal - " + HealValue;
        }
    }
}
