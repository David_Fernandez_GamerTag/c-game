﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class IronChestpiece : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 9; } }
        public override string Name { get { return "Iron Chestpiece"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.CHESTPIECE; } }

        public IronChestpiece()
        {
            Random random = new Random();
            _defenseValue = random.Next(2, 6);
        }

        public override string ToString()
        {
            return base.ToString() + " Defense - " + DefenseValue;
        }
    }
}
