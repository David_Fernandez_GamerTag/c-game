﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class IronSword : Item, IWeapon
    {
        private int _attackValue;

        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 3; } }
        public override string Name { get { return "Iron Sword"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.WEAPON; } }

        public IronSword()
        {
            Random random = new Random();

            _attackValue = random.Next(8, 15);
        }

        public override string ToString()
        {
            return base.ToString() + " Attack - " + AttackValue;
        }

    }
}
