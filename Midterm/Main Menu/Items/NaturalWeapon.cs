﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class NaturalWeapon : Item, IWeapon
    {
        private int _attackValue;

        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return true; } }
        public override double Weight { get { return 0; } }
        public override string Name { get { return "Natrual Weapon"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.WEAPON; } }

        public NaturalWeapon(int min, int max)
        {
            Random random = new Random();

            _attackValue = random.Next(min, max);
        }
    }
}
