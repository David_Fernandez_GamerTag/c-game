﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class RandomItemGenerator
    {
        private Random _random = new Random();

        public Item GenerateRandomItem()
        {
            int randomItem = _random.Next(1, 12);

            switch (randomItem)
            {
                case 1:
                    BronzeSword bronzeSword = new BronzeSword();
                    return bronzeSword;

                case 2:
                    IronChestpiece ironChestpiece = new IronChestpiece();
                    return ironChestpiece;

                case 3:
                    BronzeHelmet bronzeHelmet = new BronzeHelmet();
                    return bronzeHelmet;

                case 4:
                    IronGreaves ironGreaves = new IronGreaves();
                    return ironGreaves;

                case 5:
                    IronSword ironSword = new IronSword();
                    return ironSword;

                case 6:
                    SteelChestpiece steelChestpiece = new SteelChestpiece();
                    return steelChestpiece;

                case 7:
                    SteelHelmet steelHelmet = new SteelHelmet();
                    return steelHelmet;

                case 8:
                    BronzeVambrace bronzeVambrace = new BronzeVambrace();
                    return bronzeVambrace;

                case 9:
                    SteelGreaves steelGreaves = new SteelGreaves();
                    return steelGreaves;

                case 10:
                    SmallPotion smallPotion = new SmallPotion();
                    return smallPotion;

                case 11:
                    MasterSword masterSword = new MasterSword();
                    return masterSword;

                default:
                    return null;
            }
        }
    }
}
