﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class SteelGreaves : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 6; } }
        public override string Name { get { return "Steel Greaves"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.GREAVES; } }

        public SteelGreaves()
        {
            Random random = new Random();
            _defenseValue = random.Next(4, 7);
        }

        public override string ToString()
        {
            return base.ToString() + " Defense - " + DefenseValue;
        }
    }
}
