﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class Gambeson : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 5; } }
        public override string Name { get { return "Gambeson"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.CHESTPIECE; } }

        public Gambeson()
        {
            Random random = new Random();
            _defenseValue = random.Next(0, 4);
        }

        public override string ToString()
        {
            return base.ToString() + " Defense - " + DefenseValue;
        }
    }
}
