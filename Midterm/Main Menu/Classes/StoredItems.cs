﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class StoredItems
    {
        //Items in the bag
        private Item[] _items = new Item[20];

        //The Number of items in _item
        private int _count;

        public int Count
        {
            get { return _count; }
        }

        public StoredItems(int size)
        {
            _count = 0;
        }

        public Item GetItem(int index)
        {
            return _items[index];
        }

        public Item SetItem(int index, Item item)
        {
            item = _items[index];
            return item;
        }

        public void AddItem(Item item)
        {
            if (_count < 20)
            {
                _items[_count] = item;
                ++_count;
            }
        }

        public void RemoveItem(Item item)
        {
            int foundIndex = -1;

            for (int i = 0; i < _count; ++i)
            {
                if (item == _items[i])
                {
                    foundIndex = i;
                    break;
                }
            }
            if (foundIndex >= 0)
            {
                for (int i = 0; i <_count; ++i)
                {
                    if (i > foundIndex)
                    {
                        _items[i - 1] = _items[i];
                    }
                }
                --_count;
            }
        }

        public double CalcTotalWeight()
        {
            return 0;
        }
    }
}
