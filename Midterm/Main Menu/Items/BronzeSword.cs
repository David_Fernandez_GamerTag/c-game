﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class BronzeSword : Item, IWeapon
    {
        private int _attackValue;

        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 7; } }
        public override string Name { get { return "Bronze Sword"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.WEAPON; } }

        public BronzeSword()
        {
            Random random = new Random();
            
            _attackValue = random.Next(4, 11);
        }

        public override string ToString()
        {
            return base.ToString() + " Attack - " + AttackValue;
        }
    }
}
