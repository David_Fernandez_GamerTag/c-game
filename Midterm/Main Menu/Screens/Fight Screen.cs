﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOL
{
    public partial class FightScreen : Form
    {
        private GameManager _gameManager;
        EquippedItems equippedItems = new EquippedItems();
        public FightScreen(GameManager gameManager)
        {
            _gameManager = gameManager;
            InitializeComponent();
            gameManager.GenerateEnemy();
            lblEnemy.Text = gameManager.Enemy.Name;
            lblEnemyHealth.Text = "Health: " + gameManager.Enemy.CurrentHealth.ToString();
            lblPlayer.Text = gameManager.Player.Name;
            lblPlayerHealth.Text = "Health: " + gameManager.Player.CurrentHealth.ToString();
            lblPlayerAtk.Text = "Attack: " + (gameManager.Player.CalcTotalAttackValue()).ToString();
            lblEnemyAtk.Text = "Attack: " + (gameManager.Enemy.CalcTotalAttackValue()).ToString();
            lblPlayerDef.Text = "Defense: " + (gameManager.Player.CalcTotalDefenseValue().ToString());
            lblEnemyDef.Text = "Defense: " + (gameManager.Enemy.CalcTotalDefenseValue().ToString());
            lblDepth.Text += _gameManager.Depth;

            //test code
            equippedItems = _gameManager.Player.Equipped;

            //for (int i = 0; i < _gameManager.Player.Bag.Count; i++)
            //{
            //    lblDepth.Text = equippedItems.ToString();
            //}

            //code to disable the heal button
            if (_gameManager.Player.Equipped.GetItem(InventorySlotId.POTION1) == null)
            {
                btnPotion.Enabled = false;
            }
        }

        private void btnAttack_Click(object sender, EventArgs e)
        {
            _gameManager.Attack();
            lblEnemyHealth.Text = "Health: " + _gameManager.Enemy.CurrentHealth.ToString();
            lblPlayerHealth.Text = "Health: " + _gameManager.Player.CurrentHealth.ToString();

            if(_gameManager.Enemy.CurrentHealth <= 0)
            {
                this.Hide();
                InventoryScreen inventoryScreen = new InventoryScreen(_gameManager);
                inventoryScreen.ShowDialog();
            }
            else if (_gameManager.Player.CurrentHealth <= 0)
            {
                this.Hide();
                _gameManager.ShowGameOver();
            }
        }

        private void btnPotionOne_Click(object sender, EventArgs e)
        {
            _gameManager.Heal();
            lblPlayerHealth.Text = "Health: " + _gameManager.Player.CurrentHealth.ToString();
            _gameManager.Player.Equipped.Unequip(InventorySlotId.POTION1);
            btnPotion.Enabled = false;
        }
    }
}
