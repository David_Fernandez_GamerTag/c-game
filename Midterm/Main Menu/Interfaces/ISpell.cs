﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public interface ISpell
    {
        int CurrentCharges { get; set; }
        int MaxCharges { get; }
        int ChargesPerUse { get; }
    }
}
