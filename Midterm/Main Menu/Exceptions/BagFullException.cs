﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class BagFullException : ApplicationException
    {
        public BagFullException() : base("Bag is full")
        {

        }
    }
}
