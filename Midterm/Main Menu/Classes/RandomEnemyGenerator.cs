﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class RandomEnemyGenerator
    {
        private Random _random = new Random();
        private int _randomEnemy;

        public Character GenerateRandomEnemy(int depth)
        {
            _randomEnemy = _random.Next(1, 4);
            if (depth % 5 == 0)
            {
                Character swoopingG = new Character(22, "Swooping G");
                swoopingG.Equipped.Equip(InventorySlotId.WEAPON, new NaturalWeapon(5, 11));
                swoopingG.Equipped.Equip(InventorySlotId.CHESTPIECE, new NatrualArmor(6, 8));
                return swoopingG;
            }
            else
            {
                if (_randomEnemy == 1)
                {
                    Character rat = new Character(10, "Rat");
                    rat.Equipped.Equip(InventorySlotId.WEAPON, new NaturalWeapon(1, 4));
                    return rat;
                }
                else if (_randomEnemy == 2)
                {
                    Character bat = new Character(11, "Bat");
                    bat.Equipped.Equip(InventorySlotId.WEAPON, new NaturalWeapon(1, 4));
                    bat.Equipped.Equip(InventorySlotId.CHESTPIECE, new NatrualArmor(3, 4));
                    return bat;
                }
                else
                {
                    Character kobold = new Character(15, "Kobold");
                    kobold.Equipped.Equip(InventorySlotId.WEAPON, new BronzeSword());
                    kobold.Equipped.Equip(InventorySlotId.CHESTPIECE, new Gambeson());
                    return kobold;
                }
            }
            
        }
    }
}
