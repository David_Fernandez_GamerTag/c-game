﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public enum InventorySlotId
    {
        UNEQUIPPABLE, HELMET, CHESTPIECE, GREAVES, VAMBRACES, WEAPON, POTION1, POTION2
    }
}
