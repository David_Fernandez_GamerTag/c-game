﻿namespace LOL
{
    partial class FightScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPlayer = new System.Windows.Forms.Label();
            this.lblPlayerHealth = new System.Windows.Forms.Label();
            this.lblEnemy = new System.Windows.Forms.Label();
            this.lblEnemyHealth = new System.Windows.Forms.Label();
            this.btnAttack = new System.Windows.Forms.Button();
            this.btnPotion = new System.Windows.Forms.Button();
            this.lblDepth = new System.Windows.Forms.Label();
            this.lblPlayerAtk = new System.Windows.Forms.Label();
            this.lblEnemyAtk = new System.Windows.Forms.Label();
            this.lblPlayerDef = new System.Windows.Forms.Label();
            this.lblEnemyDef = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPlayer
            // 
            this.lblPlayer.AutoSize = true;
            this.lblPlayer.Location = new System.Drawing.Point(53, 70);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(36, 13);
            this.lblPlayer.TabIndex = 0;
            this.lblPlayer.Text = "Player";
            // 
            // lblPlayerHealth
            // 
            this.lblPlayerHealth.AutoSize = true;
            this.lblPlayerHealth.Location = new System.Drawing.Point(53, 96);
            this.lblPlayerHealth.Name = "lblPlayerHealth";
            this.lblPlayerHealth.Size = new System.Drawing.Size(44, 13);
            this.lblPlayerHealth.TabIndex = 1;
            this.lblPlayerHealth.Text = "Health: ";
            // 
            // lblEnemy
            // 
            this.lblEnemy.AutoSize = true;
            this.lblEnemy.Location = new System.Drawing.Point(159, 70);
            this.lblEnemy.Name = "lblEnemy";
            this.lblEnemy.Size = new System.Drawing.Size(39, 13);
            this.lblEnemy.TabIndex = 2;
            this.lblEnemy.Text = "Enemy";
            // 
            // lblEnemyHealth
            // 
            this.lblEnemyHealth.AutoSize = true;
            this.lblEnemyHealth.Location = new System.Drawing.Point(159, 96);
            this.lblEnemyHealth.Name = "lblEnemyHealth";
            this.lblEnemyHealth.Size = new System.Drawing.Size(41, 13);
            this.lblEnemyHealth.TabIndex = 3;
            this.lblEnemyHealth.Text = "Health:";
            // 
            // btnAttack
            // 
            this.btnAttack.Location = new System.Drawing.Point(46, 185);
            this.btnAttack.Name = "btnAttack";
            this.btnAttack.Size = new System.Drawing.Size(75, 23);
            this.btnAttack.TabIndex = 4;
            this.btnAttack.Text = "Attack";
            this.btnAttack.UseVisualStyleBackColor = true;
            this.btnAttack.Click += new System.EventHandler(this.btnAttack_Click);
            // 
            // btnPotion
            // 
            this.btnPotion.Location = new System.Drawing.Point(153, 185);
            this.btnPotion.Name = "btnPotion";
            this.btnPotion.Size = new System.Drawing.Size(75, 23);
            this.btnPotion.TabIndex = 5;
            this.btnPotion.Text = "Use Potion";
            this.btnPotion.UseVisualStyleBackColor = true;
            this.btnPotion.Click += new System.EventHandler(this.btnPotionOne_Click);
            // 
            // lblDepth
            // 
            this.lblDepth.AutoSize = true;
            this.lblDepth.Location = new System.Drawing.Point(53, 20);
            this.lblDepth.Name = "lblDepth";
            this.lblDepth.Size = new System.Drawing.Size(42, 13);
            this.lblDepth.TabIndex = 7;
            this.lblDepth.Text = "Depth: ";
            // 
            // lblPlayerAtk
            // 
            this.lblPlayerAtk.AutoSize = true;
            this.lblPlayerAtk.Location = new System.Drawing.Point(53, 123);
            this.lblPlayerAtk.Name = "lblPlayerAtk";
            this.lblPlayerAtk.Size = new System.Drawing.Size(41, 13);
            this.lblPlayerAtk.TabIndex = 8;
            this.lblPlayerAtk.Text = "Attack:";
            // 
            // lblEnemyAtk
            // 
            this.lblEnemyAtk.AutoSize = true;
            this.lblEnemyAtk.Location = new System.Drawing.Point(159, 123);
            this.lblEnemyAtk.Name = "lblEnemyAtk";
            this.lblEnemyAtk.Size = new System.Drawing.Size(41, 13);
            this.lblEnemyAtk.TabIndex = 9;
            this.lblEnemyAtk.Text = "Attack:";
            // 
            // lblPlayerDef
            // 
            this.lblPlayerDef.AutoSize = true;
            this.lblPlayerDef.Location = new System.Drawing.Point(53, 150);
            this.lblPlayerDef.Name = "lblPlayerDef";
            this.lblPlayerDef.Size = new System.Drawing.Size(50, 13);
            this.lblPlayerDef.TabIndex = 10;
            this.lblPlayerDef.Text = "Defense:";
            // 
            // lblEnemyDef
            // 
            this.lblEnemyDef.AutoSize = true;
            this.lblEnemyDef.Location = new System.Drawing.Point(159, 150);
            this.lblEnemyDef.Name = "lblEnemyDef";
            this.lblEnemyDef.Size = new System.Drawing.Size(50, 13);
            this.lblEnemyDef.TabIndex = 11;
            this.lblEnemyDef.Text = "Defense:";
            // 
            // FightScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblEnemyDef);
            this.Controls.Add(this.lblPlayerDef);
            this.Controls.Add(this.lblEnemyAtk);
            this.Controls.Add(this.lblPlayerAtk);
            this.Controls.Add(this.lblDepth);
            this.Controls.Add(this.btnPotion);
            this.Controls.Add(this.btnAttack);
            this.Controls.Add(this.lblEnemyHealth);
            this.Controls.Add(this.lblEnemy);
            this.Controls.Add(this.lblPlayerHealth);
            this.Controls.Add(this.lblPlayer);
            this.Name = "FightScreen";
            this.Text = "Fight_Screen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.Label lblPlayerHealth;
        private System.Windows.Forms.Label lblEnemy;
        private System.Windows.Forms.Label lblEnemyHealth;
        private System.Windows.Forms.Button btnAttack;
        private System.Windows.Forms.Button btnPotion;
        private System.Windows.Forms.Label lblDepth;
        private System.Windows.Forms.Label lblPlayerAtk;
        private System.Windows.Forms.Label lblEnemyAtk;
        private System.Windows.Forms.Label lblPlayerDef;
        private System.Windows.Forms.Label lblEnemyDef;
    }
}