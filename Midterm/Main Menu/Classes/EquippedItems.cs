﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class EquippedItems
    {
        private Item[] _slots = new Item[8];

        public EquippedItems()
        {
            
        }

        public Item GetItem(InventorySlotId slot)
        {
            if (slot == InventorySlotId.HELMET)
            {
                return _slots[1];
            }
            else if (slot == InventorySlotId.CHESTPIECE)
            {
                return _slots[2];
            }
            else if (slot == InventorySlotId.GREAVES)
            {
                return _slots[3];
            }
            else if (slot == InventorySlotId.VAMBRACES)
            {
                return _slots[4];
            }
            else if (slot == InventorySlotId.WEAPON)
            {
                return _slots[5];
            }
            else if (slot == InventorySlotId.POTION1)
            {
                return _slots[6];
            }
            else
            {
                return _slots[7];
            }
        }

        public Item Equip(InventorySlotId slot, Item item)
        {
            if (item.Slot == InventorySlotId.HELMET)
            {
                slot = InventorySlotId.HELMET;
                _slots[1] = item;
                return item;
            }
            else if (item.Slot == InventorySlotId.CHESTPIECE)
            {
                slot = InventorySlotId.CHESTPIECE;
                _slots[2] = item;
                return item;
            }
            else if (item.Slot == InventorySlotId.GREAVES)
            {
                slot = InventorySlotId.GREAVES;
                _slots[3] = item;
                return item;
            }
            else if (item.Slot == InventorySlotId.VAMBRACES)
            {
                slot = InventorySlotId.VAMBRACES;
                _slots[4] = item;
                return item;
            }
            else if (item.Slot == InventorySlotId.WEAPON)
            {
                slot = InventorySlotId.WEAPON;
                _slots[5] = item;
                return item;
            }
            else if (item.Slot == InventorySlotId.POTION1)
            {
                slot = InventorySlotId.POTION1;
                _slots[6] = item;
                return item;
            }
            else
            {
                slot = InventorySlotId.POTION2;
                _slots[7] = item;
                return item;
            }
        }

        public Item Unequip(InventorySlotId slot)
        {
            Item item;
            if (slot == InventorySlotId.HELMET)
            {
                item = _slots[1];
                _slots[1] = null;
                //return null;
                return item;
            }
            else if (slot == InventorySlotId.CHESTPIECE)
            {
                item = _slots[2];
                _slots[2] = null;
                //return null;
                return item;
            }
            else if (slot == InventorySlotId.GREAVES)
            {
                item = _slots[3];
                _slots[3] = null;
                //return null;
                return item;
            }
            else if (slot == InventorySlotId.VAMBRACES)
            {
                item = _slots[4];
                _slots[4] = null;
                //return item;
                return item;
            }
            else if (slot == InventorySlotId.WEAPON)
            {
                item = _slots[5];
                _slots[5] = null;
                //return item;
                return item;
            }
            else if (slot == InventorySlotId.POTION1)
            {
                item = _slots[6];
                _slots[6] = null;
                //return item;
                return item;
            }
            else
            {
                item = _slots[7];
                _slots[7] = null;
                //return item;
                return item;
            }
        }

        public double CalcTotalWeight()
        {
            return 0;
        }

        public int CalcTotalAttackValue()
        {
            //Temporary
            //return 8;

            Item it;
            int sum = 0;

            for (int i = 0; i < _slots.Length; ++i)
            {
                it = _slots[i];
                if (it is IWeapon)
                {
                    IWeapon weaponW = (IWeapon)_slots[i];
                    sum += weaponW.AttackValue;
                }
            }
            return sum;
        }

        public int CalcTotalDefenseValue()
        {
            //Temporary
            //return 0;

            Item item ;
            int sum = 0;

            for (int i = 0; i < _slots.Length; ++i)
            {
                item = _slots[i];
                if (item is IArmor)
                {
                    IArmor armor = (IArmor)_slots[i];
                    sum += armor.DefenseValue;
                }
            }
            return sum;
            
            //IArmor helmet = (IArmor)_slots[1];
            //IArmor chestpiece = (IArmor)_slots[2];
            //IArmor greaves = (IArmor)_slots[3];
            //IArmor vambraces = (IArmor)_slots[4];
            ////sum = helmet.DefenseValue + chestpiece.DefenseValue + greaves.DefenseValue + vambraces.DefenseValue;

            //int helmetDef = helmet.DefenseValue;
            //int chestpieceDef = chestpiece.DefenseValue;
            //int greavesDef = greaves.DefenseValue;
            //int vambracesDef = vambraces.DefenseValue;

            //sum = helmetDef + chestpieceDef + greavesDef + vambracesDef;
            //return sum;
        }
    }
}
