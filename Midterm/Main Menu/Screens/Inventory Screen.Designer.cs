﻿namespace LOL
{
    partial class InventoryScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCurrentHealth = new System.Windows.Forms.Label();
            this.lstBag = new System.Windows.Forms.ListBox();
            this.lstLoot = new System.Windows.Forms.ListBox();
            this.btnContinue = new System.Windows.Forms.Button();
            this.btnAddToBag = new System.Windows.Forms.Button();
            this.btnEquip = new System.Windows.Forms.Button();
            this.lblHeadText = new System.Windows.Forms.Label();
            this.lblHead = new System.Windows.Forms.Label();
            this.lblBodyText = new System.Windows.Forms.Label();
            this.lblBody = new System.Windows.Forms.Label();
            this.lblArmsText = new System.Windows.Forms.Label();
            this.lblArms = new System.Windows.Forms.Label();
            this.lblLegsText = new System.Windows.Forms.Label();
            this.lblLegs = new System.Windows.Forms.Label();
            this.lblWeaponText = new System.Windows.Forms.Label();
            this.lblWeapon = new System.Windows.Forms.Label();
            this.lblPotionText = new System.Windows.Forms.Label();
            this.lblPotion = new System.Windows.Forms.Label();
            this.lblBagCount = new System.Windows.Forms.Label();
            this.btnToss = new System.Windows.Forms.Button();
            this.lblException = new System.Windows.Forms.Label();
            this.btnUnequipWeapon = new System.Windows.Forms.Button();
            this.btnUnequipHead = new System.Windows.Forms.Button();
            this.btnUnequipBody = new System.Windows.Forms.Button();
            this.btnUnequipArms = new System.Windows.Forms.Button();
            this.btnUnequipLegs = new System.Windows.Forms.Button();
            this.btnUnequipPotion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCurrentHealth
            // 
            this.lblCurrentHealth.AutoSize = true;
            this.lblCurrentHealth.Location = new System.Drawing.Point(12, 12);
            this.lblCurrentHealth.Name = "lblCurrentHealth";
            this.lblCurrentHealth.Size = new System.Drawing.Size(81, 13);
            this.lblCurrentHealth.TabIndex = 0;
            this.lblCurrentHealth.Text = "[Current Health]";
            // 
            // lstBag
            // 
            this.lstBag.FormattingEnabled = true;
            this.lstBag.Location = new System.Drawing.Point(421, 12);
            this.lstBag.Name = "lstBag";
            this.lstBag.Size = new System.Drawing.Size(221, 121);
            this.lstBag.TabIndex = 1;
            this.lstBag.Click += new System.EventHandler(this.lstBag_Click);
            this.lstBag.SelectedIndexChanged += new System.EventHandler(this.lstBag_SelectedIndexChanged);
            // 
            // lstLoot
            // 
            this.lstLoot.FormattingEnabled = true;
            this.lstLoot.Location = new System.Drawing.Point(421, 167);
            this.lstLoot.Name = "lstLoot";
            this.lstLoot.Size = new System.Drawing.Size(221, 82);
            this.lstLoot.TabIndex = 2;
            this.lstLoot.Click += new System.EventHandler(this.lstLoot_Click);
            this.lstLoot.SelectedIndexChanged += new System.EventHandler(this.lstLoot_SelectedIndexChanged);
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(15, 246);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(75, 23);
            this.btnContinue.TabIndex = 3;
            this.btnContinue.Text = "Continue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // btnAddToBag
            // 
            this.btnAddToBag.Enabled = false;
            this.btnAddToBag.Location = new System.Drawing.Point(421, 252);
            this.btnAddToBag.Name = "btnAddToBag";
            this.btnAddToBag.Size = new System.Drawing.Size(75, 23);
            this.btnAddToBag.TabIndex = 4;
            this.btnAddToBag.Text = "Add To Bag";
            this.btnAddToBag.UseVisualStyleBackColor = true;
            this.btnAddToBag.Click += new System.EventHandler(this.btnAddToBag_Click);
            // 
            // btnEquip
            // 
            this.btnEquip.Enabled = false;
            this.btnEquip.Location = new System.Drawing.Point(421, 138);
            this.btnEquip.Name = "btnEquip";
            this.btnEquip.Size = new System.Drawing.Size(75, 23);
            this.btnEquip.TabIndex = 5;
            this.btnEquip.Text = "Equip";
            this.btnEquip.UseVisualStyleBackColor = true;
            this.btnEquip.Click += new System.EventHandler(this.btnEquip_Click);
            // 
            // lblHeadText
            // 
            this.lblHeadText.AutoSize = true;
            this.lblHeadText.Location = new System.Drawing.Point(15, 49);
            this.lblHeadText.Name = "lblHeadText";
            this.lblHeadText.Size = new System.Drawing.Size(33, 13);
            this.lblHeadText.TabIndex = 6;
            this.lblHeadText.Text = "Head";
            // 
            // lblHead
            // 
            this.lblHead.AutoSize = true;
            this.lblHead.Location = new System.Drawing.Point(71, 49);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(39, 13);
            this.lblHead.TabIndex = 7;
            this.lblHead.Text = "[None]";
            // 
            // lblBodyText
            // 
            this.lblBodyText.AutoSize = true;
            this.lblBodyText.Location = new System.Drawing.Point(15, 81);
            this.lblBodyText.Name = "lblBodyText";
            this.lblBodyText.Size = new System.Drawing.Size(31, 13);
            this.lblBodyText.TabIndex = 8;
            this.lblBodyText.Text = "Body";
            // 
            // lblBody
            // 
            this.lblBody.AutoSize = true;
            this.lblBody.Location = new System.Drawing.Point(71, 81);
            this.lblBody.Name = "lblBody";
            this.lblBody.Size = new System.Drawing.Size(39, 13);
            this.lblBody.TabIndex = 9;
            this.lblBody.Text = "[None]";
            // 
            // lblArmsText
            // 
            this.lblArmsText.AutoSize = true;
            this.lblArmsText.Location = new System.Drawing.Point(15, 110);
            this.lblArmsText.Name = "lblArmsText";
            this.lblArmsText.Size = new System.Drawing.Size(30, 13);
            this.lblArmsText.TabIndex = 10;
            this.lblArmsText.Text = "Arms";
            // 
            // lblArms
            // 
            this.lblArms.AutoSize = true;
            this.lblArms.Location = new System.Drawing.Point(71, 110);
            this.lblArms.Name = "lblArms";
            this.lblArms.Size = new System.Drawing.Size(39, 13);
            this.lblArms.TabIndex = 11;
            this.lblArms.Text = "[None]";
            // 
            // lblLegsText
            // 
            this.lblLegsText.AutoSize = true;
            this.lblLegsText.Location = new System.Drawing.Point(15, 143);
            this.lblLegsText.Name = "lblLegsText";
            this.lblLegsText.Size = new System.Drawing.Size(30, 13);
            this.lblLegsText.TabIndex = 12;
            this.lblLegsText.Text = "Legs";
            // 
            // lblLegs
            // 
            this.lblLegs.AutoSize = true;
            this.lblLegs.Location = new System.Drawing.Point(71, 143);
            this.lblLegs.Name = "lblLegs";
            this.lblLegs.Size = new System.Drawing.Size(39, 13);
            this.lblLegs.TabIndex = 13;
            this.lblLegs.Text = "[None]";
            // 
            // lblWeaponText
            // 
            this.lblWeaponText.AutoSize = true;
            this.lblWeaponText.Location = new System.Drawing.Point(15, 178);
            this.lblWeaponText.Name = "lblWeaponText";
            this.lblWeaponText.Size = new System.Drawing.Size(48, 13);
            this.lblWeaponText.TabIndex = 14;
            this.lblWeaponText.Text = "Weapon";
            // 
            // lblWeapon
            // 
            this.lblWeapon.AutoSize = true;
            this.lblWeapon.Location = new System.Drawing.Point(71, 178);
            this.lblWeapon.Name = "lblWeapon";
            this.lblWeapon.Size = new System.Drawing.Size(39, 13);
            this.lblWeapon.TabIndex = 15;
            this.lblWeapon.Text = "[None]";
            // 
            // lblPotionText
            // 
            this.lblPotionText.AutoSize = true;
            this.lblPotionText.Location = new System.Drawing.Point(15, 215);
            this.lblPotionText.Name = "lblPotionText";
            this.lblPotionText.Size = new System.Drawing.Size(37, 13);
            this.lblPotionText.TabIndex = 16;
            this.lblPotionText.Text = "Potion";
            // 
            // lblPotion
            // 
            this.lblPotion.AutoSize = true;
            this.lblPotion.Location = new System.Drawing.Point(71, 215);
            this.lblPotion.Name = "lblPotion";
            this.lblPotion.Size = new System.Drawing.Size(39, 13);
            this.lblPotion.TabIndex = 17;
            this.lblPotion.Text = "[None]";
            // 
            // lblBagCount
            // 
            this.lblBagCount.AutoSize = true;
            this.lblBagCount.Location = new System.Drawing.Point(177, 12);
            this.lblBagCount.Name = "lblBagCount";
            this.lblBagCount.Size = new System.Drawing.Size(55, 13);
            this.lblBagCount.TabIndex = 18;
            this.lblBagCount.Text = "Bag: 0/20";
            // 
            // btnToss
            // 
            this.btnToss.Enabled = false;
            this.btnToss.Location = new System.Drawing.Point(527, 138);
            this.btnToss.Name = "btnToss";
            this.btnToss.Size = new System.Drawing.Size(75, 23);
            this.btnToss.TabIndex = 19;
            this.btnToss.Text = "Toss";
            this.btnToss.UseVisualStyleBackColor = true;
            this.btnToss.Click += new System.EventHandler(this.btnToss_Click);
            // 
            // lblException
            // 
            this.lblException.AutoSize = true;
            this.lblException.ForeColor = System.Drawing.Color.Red;
            this.lblException.Location = new System.Drawing.Point(511, 257);
            this.lblException.Name = "lblException";
            this.lblException.Size = new System.Drawing.Size(59, 13);
            this.lblException.TabIndex = 20;
            this.lblException.Text = "[exception]";
            // 
            // btnUnequipWeapon
            // 
            this.btnUnequipWeapon.Location = new System.Drawing.Point(354, 173);
            this.btnUnequipWeapon.Name = "btnUnequipWeapon";
            this.btnUnequipWeapon.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipWeapon.TabIndex = 21;
            this.btnUnequipWeapon.Text = "Unequip";
            this.btnUnequipWeapon.UseVisualStyleBackColor = true;
            this.btnUnequipWeapon.Click += new System.EventHandler(this.btnUnequip_Click);
            // 
            // btnUnequipHead
            // 
            this.btnUnequipHead.Location = new System.Drawing.Point(354, 44);
            this.btnUnequipHead.Name = "btnUnequipHead";
            this.btnUnequipHead.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipHead.TabIndex = 22;
            this.btnUnequipHead.Text = "Unequip";
            this.btnUnequipHead.UseVisualStyleBackColor = true;
            this.btnUnequipHead.Click += new System.EventHandler(this.btnUnequipHead_Click);
            // 
            // btnUnequipBody
            // 
            this.btnUnequipBody.Location = new System.Drawing.Point(354, 76);
            this.btnUnequipBody.Name = "btnUnequipBody";
            this.btnUnequipBody.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipBody.TabIndex = 23;
            this.btnUnequipBody.Text = "Unequip";
            this.btnUnequipBody.UseVisualStyleBackColor = true;
            this.btnUnequipBody.Click += new System.EventHandler(this.btnUnequipBody_Click);
            // 
            // btnUnequipArms
            // 
            this.btnUnequipArms.Location = new System.Drawing.Point(354, 105);
            this.btnUnequipArms.Name = "btnUnequipArms";
            this.btnUnequipArms.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipArms.TabIndex = 24;
            this.btnUnequipArms.Text = "Unequip";
            this.btnUnequipArms.UseVisualStyleBackColor = true;
            this.btnUnequipArms.Click += new System.EventHandler(this.btnUnequipArms_Click);
            // 
            // btnUnequipLegs
            // 
            this.btnUnequipLegs.Location = new System.Drawing.Point(354, 138);
            this.btnUnequipLegs.Name = "btnUnequipLegs";
            this.btnUnequipLegs.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipLegs.TabIndex = 25;
            this.btnUnequipLegs.Text = "Unequip";
            this.btnUnequipLegs.UseVisualStyleBackColor = true;
            this.btnUnequipLegs.Click += new System.EventHandler(this.btnUnequipLegs_Click);
            // 
            // btnUnequipPotion
            // 
            this.btnUnequipPotion.Location = new System.Drawing.Point(354, 210);
            this.btnUnequipPotion.Name = "btnUnequipPotion";
            this.btnUnequipPotion.Size = new System.Drawing.Size(61, 23);
            this.btnUnequipPotion.TabIndex = 26;
            this.btnUnequipPotion.Text = "Unequip";
            this.btnUnequipPotion.UseVisualStyleBackColor = true;
            this.btnUnequipPotion.Click += new System.EventHandler(this.btnUnequipPotion_Click);
            // 
            // InventoryScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 279);
            this.Controls.Add(this.btnUnequipPotion);
            this.Controls.Add(this.btnUnequipLegs);
            this.Controls.Add(this.btnUnequipArms);
            this.Controls.Add(this.btnUnequipBody);
            this.Controls.Add(this.btnUnequipHead);
            this.Controls.Add(this.btnUnequipWeapon);
            this.Controls.Add(this.lblException);
            this.Controls.Add(this.btnToss);
            this.Controls.Add(this.lblBagCount);
            this.Controls.Add(this.lblPotion);
            this.Controls.Add(this.lblPotionText);
            this.Controls.Add(this.lblWeapon);
            this.Controls.Add(this.lblWeaponText);
            this.Controls.Add(this.lblLegs);
            this.Controls.Add(this.lblLegsText);
            this.Controls.Add(this.lblArms);
            this.Controls.Add(this.lblArmsText);
            this.Controls.Add(this.lblBody);
            this.Controls.Add(this.lblBodyText);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.lblHeadText);
            this.Controls.Add(this.btnEquip);
            this.Controls.Add(this.btnAddToBag);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.lstLoot);
            this.Controls.Add(this.lstBag);
            this.Controls.Add(this.lblCurrentHealth);
            this.Name = "InventoryScreen";
            this.Text = "Inventory_Screen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCurrentHealth;
        private System.Windows.Forms.ListBox lstBag;
        private System.Windows.Forms.ListBox lstLoot;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Button btnAddToBag;
        private System.Windows.Forms.Button btnEquip;
        private System.Windows.Forms.Label lblHeadText;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.Label lblBodyText;
        private System.Windows.Forms.Label lblBody;
        private System.Windows.Forms.Label lblArmsText;
        private System.Windows.Forms.Label lblArms;
        private System.Windows.Forms.Label lblLegsText;
        private System.Windows.Forms.Label lblLegs;
        private System.Windows.Forms.Label lblWeaponText;
        private System.Windows.Forms.Label lblWeapon;
        private System.Windows.Forms.Label lblPotionText;
        private System.Windows.Forms.Label lblPotion;
        private System.Windows.Forms.Label lblBagCount;
        private System.Windows.Forms.Button btnToss;
        private System.Windows.Forms.Label lblException;
        private System.Windows.Forms.Button btnUnequipWeapon;
        private System.Windows.Forms.Button btnUnequipHead;
        private System.Windows.Forms.Button btnUnequipBody;
        private System.Windows.Forms.Button btnUnequipArms;
        private System.Windows.Forms.Button btnUnequipLegs;
        private System.Windows.Forms.Button btnUnequipPotion;
    }
}