﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOL
{
    public partial class InventoryScreen : Form
    {
        private GameManager _gameManager;
        RandomItemGenerator _randomItem = new RandomItemGenerator();
        Item randomLoot;
        Item bossLoot1;
        Item bossLoot2;
        Item bossLoot3;

        public void LoadItems()
        {
            //Load the Equipped items
            Item helmet = _gameManager.Player.Equipped.GetItem(InventorySlotId.HELMET);
            Item chestpiece = _gameManager.Player.Equipped.GetItem(InventorySlotId.CHESTPIECE);
            Item greaves = _gameManager.Player.Equipped.GetItem(InventorySlotId.GREAVES);
            Item vambraces = _gameManager.Player.Equipped.GetItem(InventorySlotId.VAMBRACES);
            Item weapon = _gameManager.Player.Equipped.GetItem(InventorySlotId.WEAPON);
            Item potion = _gameManager.Player.Equipped.GetItem(InventorySlotId.POTION1);

            if (helmet != null)
            {
                lblHead.Text = helmet.ToString();
            }
            if (chestpiece != null)
            {
                lblBody.Text = chestpiece.ToString();
            }
            if (greaves != null)
            {
                lblLegs.Text = greaves.ToString();
            }
            if (vambraces != null)
            {
                lblArms.Text = vambraces.ToString();
            }
            if (weapon != null)
            {
                lblWeapon.Text = weapon.ToString();
            }
            if (potion != null)
            {
                lblPotion.Text = potion.ToString();
            }
        }

        public void UnequipItem(InventorySlotId slot)
        {
            if (_gameManager.Player.Bag.Count < 20)
            {
                Item item = _gameManager.Player.Equipped.Unequip(slot);
                _gameManager.Player.Bag.AddItem(item);
                lstBag.Items.Add(item);

                LoadItems();

                lblBagCount.Text = "Bag: " + _gameManager.Player.Bag.Count.ToString() + "/20";
            }
            else
            {
                throw new BagFullException();
            }
        }

        //form load
        public InventoryScreen(GameManager gameManager)
        {
            _gameManager = gameManager;
            InitializeComponent();

            lblException.Text = "";
            lblCurrentHealth.Text = "Current HP: " + gameManager.Player.CurrentHealth.ToString();

            randomLoot = _randomItem.GenerateRandomItem();
            bossLoot1 = _randomItem.GenerateRandomItem();
            bossLoot2 = _randomItem.GenerateRandomItem();
            bossLoot3 = _randomItem.GenerateRandomItem();

            if ((_gameManager.Depth % 5) - 1 == 0)
            {
                lstLoot.Items.Add(bossLoot1);
                lstLoot.Items.Add(bossLoot2);
                lstLoot.Items.Add(bossLoot3);
            }
            else
            {
                lstLoot.Items.Add(randomLoot);
            }
            

            for (int i = 0; i < _gameManager.Enemy.Bag.Count; ++i)
            {
                lstLoot.Items.Add(gameManager.Enemy.Bag.GetItem(i));
            }

            lblBagCount.Text = "Bag: " + _gameManager.Player.Bag.Count.ToString() + "/20";

            for (int i = 0; i < _gameManager.Player.Bag.Count; i++)
            {
                lstBag.Items.Add(_gameManager.Player.Bag.GetItem(i));
            }

            LoadItems();
        }

        //Move to the next enemy combat screen
        private void btnContinue_Click(object sender, EventArgs e)
        {
            this.Hide();
            FightScreen fightScreen = new FightScreen(_gameManager);
            fightScreen.ShowDialog();
        }

        //Add loot to bag
        private void btnAddToBag_Click(object sender, EventArgs e)
        {
            try
            {
                Item item = (Item)lstLoot.SelectedItem;

                if (item == null)
                {
                    throw new NoItemException();
                }

                if (_gameManager.Player.Bag.Count < 20)
                {
                    lstBag.Items.Add(item);
                    lstLoot.Items.Remove(item);
                    _gameManager.Player.Bag.AddItem(item);
                    lblBagCount.Text = "Bag: " + _gameManager.Player.Bag.Count.ToString() + "/20";
                    LoadItems();
                    lblException.Text = "";
                }
                else
                {
                    throw new BagFullException();
                }
                
            }
            catch (NoItemException ex)
            {
                lblException.Text = ex.Message;
            }
            catch (BagFullException ex)
            {
                lblException.Text = ex.Message;
            }

        }

        //Equip selected item from bag
        private void btnEquip_Click(object sender, EventArgs e)
        {
            try
            {
                Item item = (Item)lstBag.SelectedItem;
                if (item != null)
                {
                    _gameManager.Player.Equipped.Equip(item.Slot, item);

                    if (item.Slot == InventorySlotId.HELMET)
                    {
                        lblHead.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.CHESTPIECE)
                    {
                        lblBody.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.GREAVES)
                    {
                        lblLegs.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.VAMBRACES)
                    {
                        lblArms.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.WEAPON)
                    {
                        lblWeapon.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.POTION1)
                    {
                        lblPotion.Text = item.ToString();
                    }
                    if (item.Slot == InventorySlotId.POTION2)
                    {
                        lblPotion.Text = item.ToString();
                    }

                    _gameManager.Player.Bag.RemoveItem(item);
                    lstBag.Items.Remove(item);
                    LoadItems();

                    lblBagCount.Text = "Bag: " + _gameManager.Player.Bag.Count.ToString() + "/20";
                }
                else
                {
                    throw new NoItemException();
                }
            }
            catch (NoItemException ex)
            {
                lblException.Text = ex.Message;
            }
              
        }

        //Toss Item from bag
        private void btnToss_Click(object sender, EventArgs e)
        {
            try
            {
                Item item = (Item)lstBag.SelectedItem;
                if (item != null)
                {
                    lstBag.Items.Remove(lstBag.SelectedItem);
                    _gameManager.Player.Bag.RemoveItem(randomLoot);
                    lblBagCount.Text = "Bag: " + _gameManager.Player.Bag.Count.ToString() + "/20";
                    LoadItems();
                }
                else
                {
                    throw new NoItemException();
                }
            }
            catch (NoItemException ex)
            {
                lblException.Text = ex.Message;
            }
        }

        //Selecting Items in Bag
        private void lstBag_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnEquip.Enabled = true;
            btnToss.Enabled = true;
            btnAddToBag.Enabled = false;
        }

        //Selecting Items in Loot
        private void lstLoot_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAddToBag.Enabled = true;
            btnEquip.Enabled = false;
            btnToss.Enabled = false;
        }

        private void lstBag_Click(object sender, EventArgs e)
        {
            btnEquip.Enabled = false;
            btnToss.Enabled = false;
            btnAddToBag.Enabled = false;
        }

        private void lstLoot_Click(object sender, EventArgs e)
        {
            btnAddToBag.Enabled = false;
            btnEquip.Enabled = false;
            btnToss.Enabled = false;
        }

        //Unequip Head
        private void btnUnequipHead_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.HELMET);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblHead.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            }
        }

        //Unequip Body
        private void btnUnequipBody_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.CHESTPIECE);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblBody.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            } 
        }

        //Unequip Arms
        private void btnUnequipArms_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.VAMBRACES);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblArms.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            }
        }

        //Unequip Legs
        private void btnUnequipLegs_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.GREAVES);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblLegs.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            }
        }

        //Unequip Weapon
        private void btnUnequip_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.WEAPON);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblWeapon.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            } 
        }

        //Unequip Potion
        private void btnUnequipPotion_Click(object sender, EventArgs e)
        {
            try
            {
                UnequipItem(InventorySlotId.POTION1);
                if (_gameManager.Player.Bag.Count < 20)
                {
                    lblPotion.Text = "[None]";
                }
            }
            catch (Exception)
            {
                lblException.Text = "Slot is Empty";
            }
        }
    }
}
