﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class MasterSword : Item, IWeapon
    {
        private int _attackValue;

        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 20; } }
        public override string Name { get { return "Master Sword"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.WEAPON; } }

        public MasterSword()
        {
            Random random = new Random();

            _attackValue = random.Next(12, 18);
        }

        public override string ToString()
        {
            return base.ToString() + " Attack - " + AttackValue;
        }
    }
}
