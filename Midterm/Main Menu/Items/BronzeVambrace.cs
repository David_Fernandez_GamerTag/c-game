﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class BronzeVambrace : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 9; } }
        public override string Name { get { return "Bronze Vambraces"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.VAMBRACES; } }

        public BronzeVambrace()
        {
            Random random = new Random();
            _defenseValue = random.Next(0, 4);
        }

        public override string ToString()
        {
            return base.ToString() + " Defense - " + DefenseValue;
        }
    }
}
