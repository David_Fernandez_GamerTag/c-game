﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class NoItemException : ApplicationException
    {
        public NoItemException() : base ("Select an Item")
        {

        }
    }
}
