﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class SteelHelmet : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return false; } }
        public override double Weight { get { return 4; } }
        public override string Name { get { return "Steel Helmet"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.HELMET; } }

        public SteelHelmet()
        {
            Random random = new Random();
            _defenseValue = random.Next(4, 8);
        }

        public override string ToString()
        {
            return base.ToString() + " Defense - " + DefenseValue;
        }
    }
}
