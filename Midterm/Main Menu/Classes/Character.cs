﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class Character
    {
        protected StoredItems _bag;
        protected EquippedItems _equipped;
        protected int _currentHealth;
        protected bool _dead;
        protected string _name;

        public StoredItems Bag
        {
            get { return _bag; }
        }

        public EquippedItems Equipped { get { return _equipped; } }
        public int CurrentHealth { get { return _currentHealth; } }
        public bool IsDead { get { return false; } }
        public string Name { get { return _name; } }

        public Character(int health, string name)
        {
            _currentHealth = health;
            _dead = false;
            _name = name;
            _bag = new StoredItems(20);
            _equipped = new EquippedItems();
            //StoredItems storedItems = _bag;
            //EquippedItems equippedItems = _equipped;
            
        }

        public double CalcTotalWeight()
        {
            return 0;
        }

        public int CalcTotalAttackValue()
        {
            //Temporary
            //int attackValue = 8;
            //return attackValue;

            int attackValue = _equipped.CalcTotalAttackValue();
            return attackValue;
        }

        public int CalcTotalDefenseValue()
        {
            int defenseValue = _equipped.CalcTotalDefenseValue();
            return defenseValue;
        }

        public void TakeDamage(int damage)
        {
            //damage = CalcTotalAttackValue() - CalcTotalDefenseValue();

            _currentHealth -= damage;
            if (_currentHealth <= 0)
            {
                _dead = true;
                _currentHealth = 0;
            }            
        }

        public void Pickup (Item item)
        {
            _bag.AddItem(item);
        }

        public void UnequipAll()
        {
            Item item;

            item = _equipped.Unequip(InventorySlotId.HELMET);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.CHESTPIECE);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.GREAVES);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.VAMBRACES);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.WEAPON);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.POTION1);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }

            item = _equipped.Unequip(InventorySlotId.POTION2);
            if (item != null && item.IsNatural == false)
            {
                Pickup(item);
            }
        }
    }
}
