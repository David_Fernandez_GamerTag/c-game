﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class NatrualArmor : Item, IArmor
    {
        private int _defenseValue;

        public int DefenseValue { get { return _defenseValue; } }
        public override bool IsNatural { get { return true; } }
        public override double Weight { get { return 0; } }
        public override string Name { get { return "Natrual Armor"; } }
        public override InventorySlotId Slot { get { return InventorySlotId.CHESTPIECE; } }

        public NatrualArmor(int min, int max)
        {
            Random random = new Random();

            _defenseValue = random.Next(min, max);
        }
    }
}
