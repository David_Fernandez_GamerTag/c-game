﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOL
{
    public partial class StartScreen : Form
    {
        GameManager gameManager = new GameManager();
        public StartScreen()
        {
            InitializeComponent();
            

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // Code to close current form and open a new form
            //this.Close();
            FightScreen fight_Screen = new FightScreen(gameManager);
            fight_Screen.ShowDialog();
        }
    }
}
